from subprocess import check_output
import csv
import time
import matplotlib.pyplot as plt


# python
# from memory_monitor import memory_monitor
# memory_monitor(_PID_, _range_, _interval_(sec))
# if _range_ <= 0 - it is disabled


def memory_monitor(PID, range=1000, interval=1):
    memlog = []
    plt.figure()
    while True:
        mem, name = get_memory(PID)
        memlog.append(mem)
        if range > 0:
            if len(memlog) > range:
                del memlog[0]
        timestr = time.strftime('%d %b %Y %H:%M:%S')
        print('%s - %.2f' % (timestr, mem))

        plt.clf()
        plt.plot(memlog)
        plt.grid(True)
        plt.ylabel('Memory, Mb')
        plt.title('Memory usage of ' + name + ' (PID = ' + str(PID) + ')')
        plt.pause(interval)


def get_memory(PID):
    cmd_str = 'tasklist /FI "pid eq ' + str(PID) + '" /FO CSV'
    resp = check_output(cmd_str, encoding="cp866", errors="ignore")
    resp_list = list(csv.reader(resp))
    mem  = resp_list[18][0][:-3].replace(' ', '')
    name = resp_list[10][0]
    mem = float(mem)/1024.
    return mem, name


if __name__ == '__main__':

    memory_monitor(9192, range=1000, interval=1)